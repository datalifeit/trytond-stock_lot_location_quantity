# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
import datetime
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.transaction import Transaction
from trytond.wizard import StateView, Button, Wizard


__all__ = ['Lot', 'LotLocationQuantity', 'LotByLocationStart',
           'LotByLocationOpen', 'LotByLocation']

__metaclass__ = PoolMeta


class Lot:
    __name__ = 'stock.lot'

    @classmethod
    def lots_by_location(cls, lot_ids, location_ids=None,
                         with_childs=False, grouping=('product', 'lot',)):
        pool = Pool()
        Product = pool.get('product.product')
        Lot = pool.get('stock.lot')
        Location = pool.get('stock.location')

        if lot_ids:
            lots = Lot.search([('id', 'in', lot_ids)])
            product_ids = [l.product.id for l in lots]
        else:
            product_ids = None
        if not location_ids:
            locations = Location.search([('type', 'not in', ['warehouse', 'view', 'production'])])
            location_ids = [l.id for l in locations]

        pbl = Product.products_by_location(location_ids=location_ids,
                                           product_ids=product_ids,
                                           with_childs=with_childs,
                                           grouping=grouping)
        keys_todel = []
        for key, quantity in pbl.iteritems():
            if lot_ids and key[2] and key[2] not in lot_ids:
                keys_todel.append(key)
            if not key[2]:
                keys_todel.append(key)
        if keys_todel:
            for k in keys_todel:
                del pbl[k]
        return pbl


class LotLocationQuantity(ModelView):
    """Lot quantity by Location"""
    __name__ = 'stock.lot.location-quantity'

    lot = fields.Many2One('stock.lot', 'Lot', readonly=True)
    location = fields.Many2One('stock.location', 'Location',
                               required=True, readonly=True)
    quantity = fields.Float('Quantity', readonly=True)
    product = fields.Many2One('product.product', 'Product', readonly=True)
    uom = fields.Many2One('product.uom', 'Uom', readonly=True)


class LotByLocationStart(ModelView):
    """Lot By Location"""
    __name__ = 'stock.lot.location.start'

    forecast_date = fields.Date('At Date',
                                help=('Allow to compute expected stock quantities for this date.\n'
                                      '* An empty value is an infinite date in the future.\n'
                                      '* A date in the past will provide historical values.'))

    @staticmethod
    def default_forecast_date():
        Date = Pool().get('ir.date')
        return Date.today()


class LotByLocationOpen(ModelView):
    """Lot By Location"""
    __name__ = 'stock.lot.location.open'

    forecast_date = fields.Date('At Date', readonly=True)
    lot = fields.Many2One('stock.lot', 'Lot', readonly=True)
    product = fields.Function(fields.Many2One('product.product', 'Product', readonly=True,
                                              depends=['lot']),
                              'on_change_lot')
    quantities = fields.One2Many('stock.lot.location-quantity', None, 'Stock',
                                 readonly=True)

    @fields.depends('lot')
    def on_change_lot(self, name):
        if self.lot:
            return self.lot.product.id


class LotByLocation(Wizard):
    """Lot By Location"""
    __name__ = 'stock.lot.location'
    start = StateView('stock.lot.location.start',
                      'stock_lot_location_quantity.stock_lot_location_start_view_form',
                      [Button('Cancel', 'end', 'tryton-cancel'),
                       Button('Get quantities', 'open', 'tryton-ok', default=True), ])
    open = StateView('stock.lot.location.open',
                     'stock_lot_location_quantity.stock_lot_location_open_view_form',
                     [Button('Close', 'end', 'tryton-ok', default=True)])

    def default_open(self, fields):
        pool = Pool()
        Lot = pool.get('stock.lot')
        Product = pool.get('product.product')

        context = {}
        lot_id = Transaction().context['active_id']
        lot = Lot(lot_id)
        if self.start.forecast_date:
            context['stock_date_end'] = self.start.forecast_date
        else:
            context['stock_date_end'] = datetime.date.max

        with Transaction().set_context(context):
            pbl = Lot.lots_by_location(lot_ids=[lot_id],
                                       with_childs=False)

        items = []
        result = {'forecast_date': self.start.forecast_date,
                  'lot': lot_id,
                  'product': lot.product.id,
                  'quantities': items}
        for key, quantity in pbl.iteritems():
            if quantity == 0:
                continue
            ps = {'location': key[0],
                  'product': key[1],
                  'lot': key[2],
                  'quantity': quantity,
                  'uom': Product(key[1]).default_uom.id}
            items.append(ps)

        return result