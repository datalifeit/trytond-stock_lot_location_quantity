from trytond.pool import Pool
from .stock import *


def register():
    Pool.register(
        Lot,
        LotLocationQuantity,
        LotByLocationStart,
        LotByLocationOpen,
        module='stock_lot_location_quantity', type_='model')
    Pool.register(
        LotByLocation,
        module='stock_lot_location_quantity', type_='wizard')